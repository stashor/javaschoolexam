package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */


    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        try {
            int s = inputNumbers.size(), h = 0;
            while (s > 0) {  // Проверяем можно ли построить пирамиду
                h++;
                s = s - h;
            }
            if (s != 0) {
                throw new CannotBuildPyramidException();
            }

            Collections.sort(inputNumbers);
            int[][] answer = new int[h][h * 2 - 1];  // Построение пирамиды
            int pos, count = 0;
            for (int i = 0; i < h; i++){
                pos = h - i;
                for (int j = 0; j < i + 1; j++) {
                    answer[i][pos - 1] = inputNumbers.get(count);
                    count++;
                    pos += 2;
                }
            }

            return answer;
        } catch (Exception e) { // ловит ошибку брошеную в if или же при выполнении сортировки
            throw new CannotBuildPyramidException();
        }
    }
}
