package com.tsystems.javaschool.tasks.calculator;

import java.io.*;
import java.text.DecimalFormat;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            DecimalFormat df = new DecimalFormat("#.####");
            Double answer = Counting(ConvertExpression(statement));
            if (answer == Double.POSITIVE_INFINITY || answer == Double.NEGATIVE_INFINITY) {
                throw new Exception();
            }
            return df.format(answer);
        } catch (Exception e) {
            return null;
        }
    }

    static private double Counting(String input) {
        double answer = 0;
        Stack<Double> stack = new Stack<>();

        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                String a = new String("");

                while (!IsDelimeter(input.charAt(i)) && !IsOperator(input.charAt(i))) {
                    a = a + input.charAt(i);
                    i++;

                    if (i == input.length()) {
                        break;
                    }
                }
                stack.push(Double.parseDouble(a));
                i--;
            } else
            if (IsOperator(input.charAt(i))) {
                double a = stack.pop();
                double b = stack.pop();

                switch (input.charAt(i)) {
                    case '+':
                        answer = b + a;
                        break;
                    case '-':
                        answer = b - a;
                        break;
                    case '*':
                        answer = b * a;
                        break;
                    case '/':
                        answer = b / a;
                        break;
                }
                stack.push(answer);
            }
        }
        return stack.peek();
    }

    static private String ConvertExpression(String input) {
        String output = new String("");
        Stack<Character> operatorStack = new Stack<>();

        for (int i = 0; i < input.length(); i++) {
            if (Character.isDigit(input.charAt(i))) {
                while (!IsOperator(input.charAt(i))) {
                    output = output + input.charAt(i);
                    i++;

                    if (i == input.length()) {
                        break;
                    }
                }
                output += " ";
                i--;
            }

            if (IsOperator(input.charAt(i))) {
                if (input.charAt(i) == '(') {
                    operatorStack.push(input.charAt(i));
                } else
                if (input.charAt(i) == ')') {
                    char s = operatorStack.pop();

                    while (s != '(') {
                        output = output + s + ' ';
                        s = operatorStack.pop();
                    }
                } else {
                    if (!operatorStack.isEmpty()) {
                        if (GetPriority(input.charAt(i)) <= GetPriority(operatorStack.peek())) {
                            output = output + operatorStack.pop().toString() + ' ';
                        }
                    }
                    if (operatorStack.isEmpty() || GetPriority(input.charAt(i)) > GetPriority(operatorStack.peek())) {
                        operatorStack.push(input.charAt(i));
                    }
                }
            }
        }

        while (!operatorStack.isEmpty()) {
            output = output + operatorStack.pop().toString() + ' ';
        }

        return output;
    }

    static private Boolean IsOperator(char с) {
        if ("+-/*^()".indexOf(с) != -1) {
            return true;
        }
        return false;
    }

    static private Boolean IsDelimeter(char c) {
        if (" =".indexOf(c) != -1) {
            return true;
        }
        return false;
    }

    static private byte GetPriority(char c) {
        switch (c) {
            case '(': return 0;
            case ')': return 1;
            case '+': return 2;
            case '-': return 2;
            case '*': return 3;
            case '/': return 3;
            default: return 4;
        }
    }
}
