package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        if (x == null || y == null)  {
            throw new IllegalArgumentException(); // ошибка входных данных
        }
        int x_flag = 0;

        for (int i = 0; i < y.size(); i++) {
            if (x_flag >= x.size()) { // прерываем цикл, когда прошли весь List x
                break;
            }

            if (y.get(i).equals(x.get(x_flag))) { // проверяем на соответствие
                x_flag++; // увеличиваем на 1 указатель первого массива
            }
        }

        return x_flag == x.size(); // проверяем соответствия всем ли элементам первого массива найдены во втором
    }
}
